FROM locotech/convert:1.2
LABEL maintainer "John Hassel <john.hassel@locotech.fi>"
ENV TOMCAT_MAJOR="9" \
    TOMCAT_VERSION="9.0.8" \
    CONV_VERSION="0.6.0" \
    CONVSERVLET_VERSION="0.6.0" \
    CONV_URL="https://bitbucket.org/slssu/file-converter/raw/master/PDFA/files/pdfa-converter" \
    CATALINA_HOME=/opt/tomcat \
    JAVA_DOWNLOAD=http://download.oracle.com/otn-pub/java/jdk/8u172-b11/a58eab1ec242421181065cdc37240b08/jdk-8u172-linux-x64.rpm	
# Lazy download environment link, did this to avoid typing out the long URL all at once.  You can't use an ENV variable within the same ENV statement.
ENV TOMCAT_TAR=http://www.nic.funet.fi/pub/mirrors/apache.org/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz

# Update Environment:
RUN echo "LANG=en_US.utf-8" >> /etc/profile.d/locale.sh && \
    echo "LC_ALL=en_US.utf-8" >> /etc/profile.d/locale.sh && \
    echo "export LANG LC_ALL" >> /etc/profile.d/locale.sh && \
    yum -y update && \
    yum -y install python27 python27-pip gpg openssl wget unzip perl util-linux python-setuptools && \
    easy_install supervisor && \
    echo_supervisord_conf > /etc/supervisord.conf && \
    mkdir /etc/supervisor.d && \
    echo "[include]" >> /etc/supervisord.conf && \
    echo "files=/etc/supervisor.d/*.conf" >> /etc/supervisord.conf

# Copy in local configurations and utilities
COPY tomcat.conf /etc/supervisor.d/
COPY java_home.pl /opt/lts_utils/
COPY change_tomcat_id.sh /opt/lts_utils/
RUN mkdir /opt/conv && \
    useradd -u 173 -s /sbin/nologin -d /opt/tomcat -m tomcat && \
    mkdir /processing && \
    chown tomcat:tomcat /processing && \
    chown -R tomcat:tomcat /opt/lts_utils && \
    chmod +x /opt/lts_utils/* && \
    chown -R tomcat:tomcat /opt/conv

# Install Java on Amazon Linux:
RUN wget --header "Cookie: oraclelicense=accept-securebackup-cookie" $JAVA_DOWNLOAD && \
    yum localinstall -y jdk*rpm && \
    yum install -y jpackage-utils javapackages-tools

USER tomcat
# Install Converter Application
WORKDIR /opt/conv
RUN curl -o conv.zip $CONV_URL-$CONV_VERSION.zip && \
    unzip -q conv.zip && \
    rm conv.zip && \
    mv pdfa* REMOVEME && \
    mv REMOVEME/* . && \
    rmdir REMOVEME

# Install Tomcat from Apache, based on Tomcat DockerHub Package.
WORKDIR $CATALINA_HOME

RUN wget -O tomcat.tar.gz "$TOMCAT_TAR" && \
    tar -zxf tomcat.tar.gz --strip-components=1 && \
    rm bin/*.bat && \
    rm tomcat.tar.gz && \
    rm -rf webapps && \
    mkdir webapps

USER root
COPY *.properties $CATALINA_HOME/conf/
RUN chmod 775 /opt/lts_utils/* && \
    echo "JAVA_HOME=`/opt/lts_utils/java_home.pl`" >> /etc/java/java.conf && \
    chown -R tomcat:tomcat conf

USER tomcat
# Install Converter Servlet into WebApps folder as ROOT.
RUN curl -o $CATALINA_HOME/webapps/pdfa-converter-service.war $CONV_URL-$CONVSERVLET_VERSION.war && \
    mkdir $CATALINA_HOME/webapps/ROOT && \
    echo '<% response.sendRedirect("/pdfa-converter-service"); %>' > $CATALINA_HOME/webapps/ROOT/index.jsp

# Expose our Volume and Ports
VOLUME ["/processing"]
# Web Port
EXPOSE 8080 \
       8009 \
       8443

# Start up Tomcat 9
CMD ["/usr/bin/supervisord"]
